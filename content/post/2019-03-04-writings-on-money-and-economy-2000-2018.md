--- 
title: Writings on Money and Economy (2000-2018)
date: 2019-03-04 21:41:39.000000000 -05:00 
tags:
---

Compiled for a special part-issue on my work of the [Revista Sociologia
& Antropologia ](http://www.sociologiaeantropologia.com.br/)Rio de
Janeiro, 2019.

 

*Published books and articles*

2000. *The Memory Bank: money in an unequal world*, Profile Books,
    London, ix, 340p. Re-published 2001 as *Money in an Unequal World:
    Keith Hart and his memory bank*, Texere, New York and London, ix,
    341p. Available online at <http://thememorybank.co.uk/book/>

2000\. (With V. Padayachee) Indian business in South Africa after
apartheid: new and old trajectories, *Comparative Studies in Society and
History* 42. 4, 683-712

2000.  Money in an unequal world, *Anthropological Theory* 1.3, 307-
330. <http://thememorybank.co.uk/papers/money-in-an-unequal-world/>

2002a. A tale of two currencies, *Anthropology Today* 18.1, 21-22.

2002b. World society as an old regime, in C. Shore and S. Nugent (eds)
*Elite Cultures: anthropological perspectives*, Routledge, London,
22-36.
<http://thememorybank.co.uk/papers/world-society-as-an-old-regime/>

2002c. Anthropologists and development, *Norsk Antropologisk Tijdskrift*
13. 1-2, 14-21.

2002d. Quelques confidences sur l'anthropologie du développement,
*ethnographiques.org* No. 2 Nov.,
<http://www.ethnographiques.org/2002/Hart.html>

2004a. Towards a global green currency, *Ecology & Farming*
(June-August).  Available online at
<http://thememorybank.co.uk/2004/09/15/organic-trade-towards-a-global-green-currency/>.

2004b. Notes towards an anthropology of the internet, *Horizontes
Antropologicos* 10. 2: 15-40.

2004c. The political economy of food in an unequal world, in M Lien and
B. Nerlich (eds) *Politics of Food*, Berg, Oxford, 199-220.
<http://thememorybank.co.uk/papers/the-political-economy-of-food/>

2004. (Editor with J. Bryden) *A New Approach to Rural Development in
    Europe: Germany, Greece, Scotland, Sweden*, Mellen Studies in
    Geography Volume 9, Edwin Mellen Press, Lewiston NY, xxvii, 403p.

2005a. *The Hit Man's Dilemma: or business, personal and impersonal*, 
Prickly Paradigm Press, Chicago, 117p.

2005b. Money: one anthropologist's view, in J. Carrier (ed) *Handbook of
Economic Anthropology*, Edward Elgar, Cheltenham, 160-175.
<http://thememorybank.co.uk/papers/money-one-anthropologists-view/>

2006a. Bureaucratic form and the informal economy, in B. Guha-Khasnobis,
R. Kanbur and E. Ostrom (eds) *Linking the Formal and Informal
Economies*, Oxford University Press, Oxford, 21-35.
<http://thememorybank.co.uk/papers/bureaucratic-form-and-the-informal-economy/>

2006b. Richesse commune: construire une démocratie économique à l'aide
des monnaies communautaires, in Jérôme Blanc (ed) *Exclusion et liens
financiers* -- "Monnaies sociales" Economica, Paris, 135-152.

2007a. Towards an alternative economics: the view from France, *Social
Anthropology*, 15(3), 369-374.

2007b. Money is always personal and impersonal, *Anthropology Today*
23(5), 16-20.

2007c. Interview: Keith Hart answers questions on economic anthropology,
*European Economic Sociology Newsletter* 9 (1), 11-16.

2007d. Money and anthropology: object, theory and method in E. Baumann
*et al* (Editors) Argent des anthropologues, monnaie des économistes.
Paris: Harmattan.

2008. The human economy,
    [*ASAonline*](http://www.theasa.org/publications/asaonline/articles/asaonline_art1.htm).
2009. (With H. Ortiz) Anthropology in the financial crisis,
    *Anthropology Today* 24.6 1-3.

2009a. The persuasive power of money in S. Gudeman ed *Economic
Persuasions*, Berghahn, Oxford.
<http://thememorybank.co.uk/2007/01/12/the-persuasive-power-of-money/>

2009b. Money in the making of world society in C. Hann and K. Hart eds
*Market and Society*, Cambridge University Press, Cambridge..

2009. (Editor with C. Hann) *Market and Society: The Great
    Transformation Today*, Cambridge University Press.

2010a. King for a day, *The Big Issue* (December).

2010b. Mauss et sa vision de l'économie dans les années 1920-25, *Revue
du MAUSS* No. 36, 34-48.

2010c. The legal order of gift-giving (Review of R. Hyland *Gifts: A
study in comparative law*, Oxford 2009), *European Journal of Sociology*
51:3, 559-564.

2010d. Africa's urban revolution and the informal economy, in V.
Padayachee (ed) *The Political Economy of Africa*, Routledge: London,
371-388.

2010. (Editor with J-L. Laville and A. D. Cattani) *The Human Economy: A
    citizen's guide*, Polity Press, Cambridge.

2010a. (With V. Padayachee) Introducing the African economy, in V.
Padayachee (ed) *The Political Economy of Africa*, London: Routledge.

2010b. (With V. Padayachee) South Africa in Africa: from national
capitalism to regional integration, in V. Padayachee (ed) *The Political
Economy of Africa*, London: Routledge.

2011a. Money as a form of religious life. *Religion and Society:
Advances in Research* 1: 156--63.

2011b. The financial crisis and the end of all-purpose money, *Economic
Sociology: the European Electronic Newsletter* 12.2 4-10.

2011c. Building the human economy: a question of value? *Suomen
Antropologi: Journal of the Finnish Anthropological Society* 36(2): 5-17

2011d. After the financial crisis: towards a human economy, *Norsk
Antropologisk Tidsskrift* 3-4, 181-193.

2011. (With C. Hann) *Economic Anthropology: History, Ethnography,
    Critique*, Polity Press, Cambridge.

2012a. Anthropology, economics and development in Richard Fardon et al
(editors) *The Sage Handbook of Social Anthropology* Volume 1. Sage,
London, 154-167.

2012b. Money in twentieth century anthropology in James G Carrier
(editor) *A Handbook of Economic Anthropology*, *Second edition*, Edward
Elgar, Cheltenham,166-182.

2012c. The financial crisis and the history of money in James G Carrier
(editor), as above, 626-637.

2013. Making money with money: reflections of a betting man, in R.
    Cassidy, A. Pisac and C. Loussouarn (eds) *Qualitative Research in
    Gambling: Exploring the production and consumption of risk*,
    Routledge, London, 15-27.
2014. (with V. Padayachee) A history of South African capitalism in
    national and global perspective, *Transformation* 81/82: 55-85.

2014a. Marcel Mauss's economic vision, 1920-1925: anthropology,
politics, journalism, *Journal of Classical Sociology* 14.1: 34-44.

2014b. Jack Goody: the anthropology of unequal society, *Reviews in
Anthropology* 43.3: 199-220.

2014c. The rise and fall of Europe, *Economic and Political Weekly*
49.34 (August 23): 27-30.

2014d. (with H. Ortiz) The anthropology of money and finance: between
ethnography and world history, *Annual Review of Anthropology*
43:465-482.

2014e. (with J. Sharp and V. Laterza) South Africa in world development:
Prospects for a human economy. Anthropology Today, 30: 13--17.

2014. (Editor with J. Sharp) *People, Money and Power in the Economic
    Crisis: Perspectives from the Global South*, Berghahn, New York.

2015a. (Editor) *Economy For and Against Democracy*, Berghahn, New York.

2015b. How the informal economy took over the world, in P.Moertenboeck,
H. Mooshammer, T, Cruz and F. Forman (eds) Informal Market Worlds
Reader: The architecture of economic pressure. NAI010 Publishers, 33-44.

2015c. Money in the making of world society. In Lovink, G., Tkacz, N.
and de Vries, P. (eds) *Moneylab Reader: An intervention in digital
economy*. Amsterdam: Institute for Network Cultures, 19-31.

2015. (With John Bryden) Money and banking in Scotland and Norway, in J.
    Bryden, O. Brox and L. Riddock (Editors) *Northern Neighbors:
    Scotland and Norway since 1800*. Edinburgh: Edinburgh University
    Press, 164-187.

2016a. The anthropology of debt, *Journal of the Royal Anthropological
Institute* 22.2: 415-421.

2016b. Recent transformations in how anthropologists study money,
*Journal of the Royal Anthropological Institute* 22.3: 712-716.

2017a. (Editor) *Money in a Human Economy*, Berghahn, New York

2017b. Capitalism and our moment in the history of money, Chapter 1, K.
Hart ed *Money in a Human Economy.*

2017c. Stephen Gudeman, Anthropology and the Economy, *Anthropological
Forum*.

2017d. Greybacks in B. Maurer and L. Swartz Editors *Paid: Tales of
dongles, checks and other  money stuff*. Cambridge: MIT Press, 211-220.

2018. Market fundamentalism at the crossroads, *Cultural Anthropology*
    (November).

<!-- -->

2018. (With R. Kaur and J. Comaroff) Southern Futures: Thinking through
    emerging markets, *Comparative Studies of South Asia, Africa and the
    Middle East* 38.2:365-376

*Unpublished papers at*
[https://universitypretoria.](https://universitypretoria.academia.edu/KeithHart/Drafts)[academia.edu/KeithHart/Drafts](https://universitypretoria.academia.edu/KeithHart/Drafts)

 

Africa's Old Regimes are ripe for liberal revolution

Exchange in a human economy

Free trade and protection

Informal economy, the Great Transformation and the humanity of
corporations

Informality and international trade: the case for an African customs
union

Learning about money

Money in the making of a human economy: beyond national capitalism

Movement as a human right

My experience of the development industry in the 1970s

Polanyi, Marx and double movement then and now

Preface to the coming world crisis

Private property and the corporations

Religion and economy

Taking the human economy idea forward on a broader basis

The euro crisis: an episode in the global history of money

The real economy: the challenge of dialectical method

What might an anthropology of the internet look like?

 

*Unpublished papers at <http://thememorybank.co.uk>*

\* *A conversation with Dave Birch about the future of money

A short history of economic anthropology

A tale of two currencies: the euro and the Argentinian peso

African enterprise and the informal economy

Alternative currencies at Limehouse 2005

Anti-capitalism

Between slavery and emancipation in West Africa

Building economic democracy with community currencies

Building the human economy

Cambridge lecture on international development (2009 video)

Capitalism: making money with money

Capitalism: the political economy of development

Globalization from below: the world's other economy (new preface)

Iceland's revenge

IE + IT = ED

In Rousseau's footsteps: David Graeber and the anthropology of unequal
society

Informality: problem or solution?

Intangible money matters

Intellectual property

Jack Goody's vision of world history and African development today

Kinship, contract and trust

Manifesto for a human economy

Marx between Mill, Mayhew and Dickens

Mediation and memory in the theory of money

Money and anthropology: object, theory and method

Money is always social, global and virtual

On commoditization: exchange in the human economy

Opening anthropology: an interview with Keith Hart

Peopled economies

Studying world society as a vocation

The African revolution: development in the 21^st^ century world

The changing character of money

The future of money and the market

The Hit Man's Dilemma (lite)

The hope and reality of money

The informal economy: a story of ethnography untold

The limits of naivety for the study of money

The machine revolution today

The market from a humanist point of view

The origins of money: cows and shells

The politics, pragmatics and promise of money

The privatization of public interests

The urban informal economy in retrospect

Two lectures on African development

Two pieces on money for a Japanese magazine

Varieties of community currency

Varieties of national economy

Waiting for emancipation: the prospects for liberal revolution in Africa

West Africa's political economy: a regional history

\* *

 

 

 
